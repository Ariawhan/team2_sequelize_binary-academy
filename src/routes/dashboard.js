const router = require("express").Router();
const { controllersDashboard } = require("../controllers/index");

//Data
// router.get("/findData/:id"), controllersDashboard.findData);
router.put("/updateData/:id", controllersDashboard.updateData);
router.delete("/deleteData/:id", controllersDashboard.deleteData);
router.post("/insertData/:id", controllersDashboard.insertData);

module.exports = router;
