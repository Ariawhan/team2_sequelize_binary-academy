const router = require("express").Router();
const controllersAuth = require("../controllers/auth");

// api login
router.post("/auth/login", controllersAuth.authLogin);

//api register
router.post("/auth/register", controllersAuth.createUser);

module.exports = router;
