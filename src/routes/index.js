const router = require("express").Router();
const auth = require("./auth");
const dashboard = require("./dashboard");
const { controllersDashboard } = require("../controllers/index");

//view Engine
//auth
// Halaman login
router.get("/", (req, res) => {
  res.render("../views/auth/login");
});
// halaman register
router.get("/register", (req, res) => {
  res.render("../views/auth/register");
});

// halaman user
router.get("/users",  controllersDashboard.getMovie);
router.get("/users/orders",  controllersDashboard.getOrders);

// halaman admin
router.get("/admin",  controllersDashboard.getAdmin);
router.get("/admin/users",  controllersDashboard.getUsers);
// router.get("/admin",  controllersDashboard.getAdmin);
// router.get("/admin",  controllersDashboard.getAdmin);


// server api
router.use("/api", dashboard);
router.use("/api", auth);

module.exports = router;
