const { products } = require("../models");
const { users } = require("../models");
const { bookings } = require("../models");
const { Op } = require("sequelize");
const sequelize = require("sequelize");
const { QueryTypes } = require("sequelize");

const getCookie = (name, req) => {
  console.log(req);
  var cookieArr = req.split(";");
  for (var i = 0; i < cookieArr.length; i++) {
    var cookiePair = cookieArr[i].split("=");
    if (name == cookiePair[0].trim()) {
      // console.log(decodeURIComponent(cookiePair[1]))
      return decodeURIComponent(cookiePair[1]);
    }
  }
};

// Get data
const getAdmin = async (req, res) => {
  if (req.query.key !== undefined) {
    const data = req.query.key.toLowerCase();
    try {
      // SELECT SUM(CAST(products.price as int)) as total
      // FROM bookings
      // INNER JOIN products ON bookings.id_film = products.id
      const totalMovie = await products.findAll({});
      const totalUser = await users.findAll({});
      const totalBooking = await bookings.findAll({});
      // const totalMovie = await sequelize.query("SELECT max(id) as total_movie FROM products");
      console.log(totalMovie);
      const searchResult = await products.findAll({
        where: sequelize.where(
          sequelize.fn("lower", sequelize.col("name")),
          "Like",
          "%" + data + "%"
        ),
      });
      console.log(req.headers);
      res.render("../views/admin/dashboard", {
        status: true,
        message: "Data Movie",
        data: searchResult,
        username: getCookie("username", req.headers.cookie),
        role: getCookie("role", req.headers.cookie),
        idUsers: getCookie("id", req.headers.cookie),
        token: getCookie("token", req.headers.cookie),
        totalMovie: totalMovie.length,
        totalUser: totalUser.length,
        totalBooking: totalBooking.length,
      });
    } catch (err) {
      res.status(400).json({
        status: "fail",
        errors: [err.message],
      });
    }
  }
  // console.log(req.query.key);
  else {
    try {
      const movieData = await products.findAll({});
      const totalMovie = await products.findAll({});
      const totalUser = await users.findAll({});
      const totalBooking = await bookings.findAll({});
      console.log(getCookie("username", req.headers.cookie));
      // console.log("else ..");
      res.render("../views/admin/dashboard", {
        status: true,
        message: "Data Movie",
        data: movieData,
        username: getCookie("username", req.headers.cookie),
        role: getCookie("role", req.headers.cookie),
        idUsers: getCookie("id", req.headers.cookie),
        token: getCookie("token", req.headers.cookie),
        totalMovie: totalMovie.length,
        totalUser: totalUser.length,
        totalBooking: totalBooking.length,
      });
    } catch (err) {
      res.status(400).json({
        status: "fail",
        errors: [err.message],
      });
    }
  }
};

// find Data
// const findData = async (req, res) => {
//   if (req.params.id == "findMovie") {
//     //Kondisi
//   } else if (req.params.id == "findUsers") {
//   }
// };

const getUsers = async (req, res) => {
  try {
    const usersData = await users.findAll({});
    // console.log(usersData);
    // console.log(usersData);
    res.render("../views/admin/users", {
      status: true,
      message: "Data Users",
      data: usersData,
      username: getCookie("username", req.headers.cookie),
      role: getCookie("role", req.headers.cookie),
      idUsers: getCookie("id", req.headers.cookie),
      token: getCookie("token", req.headers.cookie),
    });
  } catch (err) {
    res.status(400).json({
      status: "fail",
      errors: [err.message],
    });
  }
};

const getMovie = async (req, res) => {
  // console.log(document.cookie.id);
  try {
    const movieData = await products.findAll();
    // console.log(req.headers.cookie);
    // console.log(movieData[1].dataValues.name)
    res.render("../views/customer/cards", {
      status: true,
      message: "Data Movie",
      data: movieData,
      username: getCookie("username", req.headers.cookie),
      role: getCookie("role", req.headers.cookie),
      idUsers: getCookie("id", req.headers.cookie),
      token: getCookie("token", req.headers.cookie),
    });
  } catch (err) {
    res.status(400).json({
      status: "fail",
      errors: [err.message],
    });
  }
};

const getOrders = async (req, res) => {
  try {
    // SELECT products.name, products.price, bookings.card
    // FROM bookings
    // INNER JOIN products ON bookings.id_film = products.id
    // WHERE bookings.user_id = 2
    // const ordersData = await bookings.findAll({});
    const users = await sequelize.query("SELECT * FROM `bookings`", {
      type: QueryTypes.SELECT,
    });
    res.render("../views/customer/oders", {
      status: true,
      message: "Data Orders",
      data: ordersData,
      username: getCookie("username", req.headers.cookie),
      role: getCookie("role", req.headers.cookie),
      idUsers: getCookie("id", req.headers.cookie),
      token: getCookie("token", req.headers.cookie),
    });
  } catch (err) {
    res.status(400).json({
      status: "fail",
      errors: [err.message],
    });
  }
};

// Update data
const updateData = async (req, res) => {
  if (req.params.id === "editMovie") {
    const idData = req.body.id;
    const data = {
      name: req.body.name,
      start_date: req.body.startMovie,
      end_date: req.body.endMovie,
      price: req.body.seat,
      images: req.body.images,
      desc: req.body.description,
    };
    try {
      const usersData = await products.update(data, {
        where: {
          id: idData,
        },
      });
      if (parseInt(usersData) == 0) {
        console.log(parseInt(usersData));
        res.status(200).json({
          status: false,
          message: `Invalid id movie ${id}`,
        });
      } else {
        console.log(usersData);
        res.status(200).json({
          status: true,
          message: "Movie success edited",
        });
      }
    } catch (err) {
      res.status(400).json({
        status: "fail",
        errors: [err.message],
      });
    }
  } else if (req.params.id === "changeRole") {
    // Query p = 1 -> change to Admin
    // p = 2 -> change to Users
    const idData = req.query.q;
    console.log(idData);
    console.log(parseInt(req.query.p));
    switch (parseInt(req.query.p)) {
      case 1:
        try {
          const role = await users.update(
            {
              role: 1,
            },
            {
              where: {
                id: idData,
              },
            }
          );
          console.log(role);
          res.status(200).json({
            status: true,
            message: "Successful change role to Admin",
          });
        } catch (err) {
          res.status(400).json({
            status: "fail",
            errors: [err.message],
          });
        }
        break;
      case 2:
        try {
          const role = await users.update(
            {
              role: 0,
            },
            {
              where: {
                id: idData,
              },
            }
          );
          console.log(role);
          res.status(200).json({
            status: true,
            message: "Successful change role to user",
          });
        } catch (err) {
          res.status(400).json({
            status: "fail",
            errors: [err.message],
          });
        }
        break;
    }
  } else if (req.params.id === "status") {
    // Query q = 1 -> bloks
    // q = 2 -> unblok
    const idData = req.query.p;
    console.log(idData);
    console.log(parseInt(req.query.q));
    switch (parseInt(req.query.q)) {
      case 1:
        try {
          const role = await users.update(
            {
              status: 1,
            },
            {
              where: {
                id: idData,
              },
            }
          );
          console.log(role);
          res.status(200).json({
            status: true,
            message: "Successful blokir",
          });
        } catch (err) {
          res.status(400).json({
            status: "fail",
            errors: [err.message],
          });
        }
        break;
      case 2:
        try {
          const role = await users.update(
            {
              status: 0,
            },
            {
              where: {
                id: idData,
              },
            }
          );
          console.log(role);
          res.status(200).json({
            status: true,
            message: "Successful unblokir",
          });
        } catch (err) {
          res.status(400).json({
            status: "fail",
            errors: [err.message],
          });
        }
        break;
    }
  }
};

// Delete data
const deleteData = async (req, res) => {
  try {
    const id = req.params.id;
    console.log(id);
    const deleted = await products.destroy({
      where: {
        id,
      },
    });
    if (deleted == 0) {
      console.log(deleted);
      res.status(200).json({
        status: false,
        message: `Invalid id movie ${id}`,
      });
    } else {
      res.status(200).json({
        status: true,
        message: `Successful deleted movie id ${id}`,
      });
    }
  } catch (err) {
    res.status(400).json({
      status: "fail",
      errors: [err.message],
    });
  }
};

// Insert data
const insertData = async (req, res) => {
  if (req.params.id === "addMovie") {
    const data = {
      name: req.body.name,
      start_date: req.body.startMovie,
      end_date: req.body.endMovie,
      price: req.body.seat,
      images: req.body.images,
      desc: req.body.description,
    };
    try {
      const newProduct = await products.create(data);
      res.status(201).json({
        status: true,
        message: "Movie created successfully",
        data: {
          newProduct,
        },
      });
    } catch (err) {
      res.status(400).json({
        status: "fail",
        errors: [err.message],
      });
    }
  } else if (req.params.id === "ordersMovie") {
    if (req.body.wibuCards.length == 16) {
      const data = {
        id_film: req.body.idFilm,
        user_id: req.body.userId,
        card: req.body.wibuCards,
      };
      try {
        const booking = await bookings.create(data);
        res.status(201).json({
          status: true,
          message: "Booking created successfully",
          data: {
            booking,
          },
        });
      } catch (err) {
        res.status(400).json({
          status: "fail",
          errors: [err.message],
        });
      }
    } else {
      res.status(201).json({
        status: false,
        message: "Invalid Card Number",
      });
    }
  }
};

module.exports = {
  getAdmin,
  // findData,
  getUsers,
  getMovie,
  getOrders,
  updateData,
  deleteData,
  insertData,
};
