const { users } = require("../models");
const md5 = require("md5");
const { Op } = require("sequelize");
const axios = require("axios");

const authLogin = async (req, res) => {
  const data = {
    username: req.body.username,
    password: md5(req.body.password),
  };
  console.log(res.body);
  try {
    // SELECT username, id, token FROM users WHERE username='ariawan' && password="asas"
    const user = await users.findOne({
      attributes: ["username", "id", "token", "role", "status"],
      where: {
        [Op.and]: [{ username: data.username }, { password: data.password }],
      },
    });
    if (user == null) {
      res.status(200).json({
        status: false,
        message: "Username or Password incorrect!",
      });
    } else {
      res.status(200).json({
        status: true,
        message: "Login Successfully",
        data: {
          user,
        },
      });
    }
  } catch (err) {
    res.status(400).json({
      status: "fail",
      errors: [err.message],
    });
  }
};

// INSERT INTO "users" ("id","username", "password", "token", "createdAt", "updatedAt") VALUES (DEFAULT,'Ramadhan', 'e10adc3949ba59abbe56e057f20f883e', '34e1b5f421250aaa4ef44e30a9eb0a64', NOW(),NOW())
const createUser = async (req, res) => {
  const data = {
    username: req.body.username,
    password: req.body.password,
  };
  // req.body.username, req.body.price, req.body.quantity
  try {
    const newUser = await users.create({
      username: data.username,
      password: md5(data.password),
      status: 0, // Status di blokir apa tidak (tambah di database)
      role: 0, // role admin apa gak admin (tambah di daabase)
      token: md5(md5(data.username) + md5(data.password) + md5(new Date())),
    });
    // const output = newUser.dataValues.id;
    res.status(201).json({
      status: true,
      message: "Registration Successfully",
      data: {
        id: newUser.dataValues.id,
        username: newUser.dataValues.username,
        token: newUser.dataValues.token,
        role: newUser.dataValues.role,
      },
    });
  } catch (err) {
    res.status(400).json({
      status: "fail",
      errors: [err.message],
    });
  }
};

module.exports = {
  authLogin,
  createUser,
};
